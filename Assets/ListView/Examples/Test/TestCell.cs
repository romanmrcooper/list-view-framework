﻿using UnityEngine.UI;

namespace Assets.ListView.Examples.Test
{
    public class TestCell : BaseCell
    {
        public Text Text;
        
        public override void Setup(object vlaue)
        {
            base.Setup(vlaue);

            Text.text = (vlaue as FriendData).Name;
        }
    }
}