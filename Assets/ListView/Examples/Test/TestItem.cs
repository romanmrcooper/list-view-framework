﻿using ListView;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.ListView.Examples.Test
{
    public class TestItem : ListViewItem<TestItemData>
    {
        public Text Text;

        public override void Setup(TestItemData data)
        {
            base.Setup(data);
            Text.text = data.SomeData;
        }
    }
}