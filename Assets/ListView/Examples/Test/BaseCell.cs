﻿using UnityEngine;

namespace Assets.ListView.Examples.Test
{
    [RequireComponent(typeof (RectTransform))]
    public class BaseCell : MonoBehaviour
    {
        protected object _data;

        public string TemplateName { get; set; }

        public virtual void Setup(object value)
        {
            _data = value;
        }

        public RectTransform RectTransform
        {
            get { return transform as RectTransform;}
            private set { }
        }

        protected virtual void OnDestroy()
        {
            // for override
        }
    }
}