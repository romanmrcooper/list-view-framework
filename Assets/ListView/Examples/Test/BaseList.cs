﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.ListView.Examples.Test
{
    [RequireComponent(typeof (RectTransform))]
    public abstract class BaseList<TData, TCell> : MonoBehaviour where TCell : BaseCell
    {
        /*
         * + todo padding (top, bottom)
         * + gap
         * + on lists destroy should destroy all: pool and visible
         * + elastic edges
         * + проверять ячейку на относительные размеры при гориз\верт напрявлении
         */
        private readonly Dictionary<string, CellPool> _pools = new Dictionary<string, CellPool>();
        private readonly Border _startBorder = new Border();

        private readonly Dictionary<string, Vector3> _templateSizes = new Dictionary<string, Vector3>();
        private List<TData> _data;
        private float _lastScrollOffset;
        private float _viewportEndPos;
        public Dictionary<int, TCell> DataToCellMap = new Dictionary<int, TCell>();

        public Directions Direction;
        private bool _isSettedUp;
        private Vector3 _leftSide;

        public float Gap = 0f;
        public float PaddingTop = 0f;
        public float PaddingBottom = 0f;

        public float PendingScrollOffset;
        public Func<int, TData, string> TemplateQualifier;
        private RectTransform _rectTransform;


        public float ScrollOffset;

        public BaseCell[] Templates;

        public int Axis
        {
            get { return IsHorizontal ? 0 : 1; }
        }

        public bool IsVertical
        {
            get { return Direction == Directions.Vertial; }
        }

        public bool IsHorizontal
        {
            get { return !IsVertical; }
        }

        public List<TData> Data
        {
            get { return _data; }
            set
            {
                _data = value;
                OnSetDataProviderComplete();
            }
        }

        private void Start()
        {
            _rectTransform = transform as RectTransform;

            Setup();

            _isSettedUp = true;

            OnSetupComplete();
        }

        private void Update()
        {
            if (!_isSettedUp)
            {
                return;
            }

            ComputeConditions();
//            UpdateItems();
        }

        private void Setup()
        {
            if (Templates.Length == 0)
            {
                throw new Exception("No cell templates! At least 1 required");
            }

            foreach (var template in Templates)
            {
                if (_pools.ContainsKey(template.name))
                {
                    throw new Exception("Two templates cannot have the same name");
                }

                if (IsVertical)
                {
                    if (template.RectTransform.anchorMin.y <= 0 || template.RectTransform.anchorMax.y >= 1)
                        throw new Exception("In vertical direction cell Y anchors must be in range of (0..1)");
                }
                else
                {
                    if (template.RectTransform.anchorMin.x <= 0 || template.RectTransform.anchorMax.x >= 1)
                        throw new Exception("In horizontal direction cell X anchors must be in range of (0..1)");
                }

                _pools[template.name] = new CellPool(template);
                _templateSizes[template.name] = template.RectTransform.rect.size;
            }

            _viewportEndPos = IsVertical ? _rectTransform.rect.height : _rectTransform.rect.width;
        }
        
        private void ComputeConditions()
        {
            if (Data == null || Data.Count == 0)
            {
                return;
            }

            if (IsVertical)
            {
                _leftSide = Vector3.down * PaddingTop;
            }
            else
            {
                _leftSide = Vector3.right * PaddingTop;
            }
        }

        private void OnSetupComplete()
        {
            FirstStart();
        }

        private void OnSetDataProviderComplete()
        {
            FirstStart();
        }

        private void FirstStart()
        {
            if (_isSettedUp && Data != null && Data.Count > 0)
            {
                _startBorder.Index = 0;
                _startBorder.Position = 0;

                Bottom.Index = Data.Count - 1;
                Bottom.Position = GetBottomPos();

                ComputeConditions();

                CleanTop();
            }
        }

        private float GetBottomPos()
        {
            float res = 0f;
            float gap = 0;
            for (int i = 0; i < Data.Count; i++)
            {
                gap = (i == Data.Count - 1) ? 0 : Gap;
                res -= GetObjectSize(Data[i])[Axis] + gap;
            }
            return res + _viewportEndPos - PaddingBottom;
        }

        private void RecycleItem(TData data)
        {
            RecycleItem(Data.IndexOf(data));
        }

        private void RecycleItem(int index)
        {
            if (!DataToCellMap.ContainsKey(index))
            {
                return;
            }

            TCell cell = DataToCellMap[index];
            DataToCellMap.Remove(index);
            _pools[cell.TemplateName].Pool.Add(cell);
            cell.gameObject.SetActive(false);
        }

        private void Positioning(BaseCell cell, float offset)
        {
            Vector3 position;
            if (IsVertical)
            {
                position = _leftSide + offset * Vector3.down;
            }
            else
            {
                position = _leftSide + offset*Vector3.right;
            }
            cell.RectTransform.localPosition = position;
        }

        private Vector3 GetObjectSize(string templateName)
        {
            return _templateSizes[templateName];
        }

        private Vector3 GetObjectSize(TData data)
        {
            var templateName = TemplateQualifier.Invoke(Data.IndexOf(data), data);
            return GetObjectSize(templateName);
        }

        private void ExtremeLeft(TData data)
        {
            RecycleItem(data);
        }

        private void ExtremeRight(TData data)
        {
            RecycleItem(data);
        }

        private void ListMiddle(TData data, float offset)
        {
            var index = Data.IndexOf(data);
            TCell cell;
            if (DataToCellMap.ContainsKey(index))
            {
                cell = DataToCellMap[index];
                cell.gameObject.SetActive(true);
            }
            else
            {
                cell = GetCell(data);
                DataToCellMap[index] = cell;
            }

            Positioning(cell, offset);
        }

        private string QualifyTemplateName(TData data)
        {
            return TemplateQualifier.Invoke(Data.IndexOf(data), data);
        }

        private TCell GetCell(TData data)
        {
            var templateName = QualifyTemplateName(data);
            if (!_pools.ContainsKey(templateName))
            {
                throw new Exception("Cannot get item, template \"" + templateName + "\" doesn't exist");
            }
            TCell cell;
            if (_pools[templateName].Pool.Count > 0)
            {
                cell = (TCell) _pools[templateName].Pool[0];
                _pools[templateName].Pool.RemoveAt(0);

                cell.gameObject.SetActive(true);
            }
            else
            {
                cell = (TCell) Instantiate(_pools[templateName].Prefab).GetComponent<TCell>();
                cell.TemplateName = templateName;
                cell.transform.SetParent(transform, false);
            }
            cell.Setup(data);
            return cell;
        }

        public void ScrollTo(TData data)
        {
            var index = Data.IndexOf(data);
            if (index == -1)
            {
                throw new Exception("Value is not from list's data provider");
            }
            ScrollTo(index);
        }

        public void ScrollTo(int index)
        {
            index = Mathf.Clamp(index, 0, Data.Count);

            var size = 0f;
            bool isLast;
            float gapOffset;
            int i;

            if (index == _startBorder.Index)
            {
                // do nothing
            }
            else if (index < _startBorder.Index)
            {
                for (i = _startBorder.Index - 1; i >= index; i--)
                {
                    isLast = i == (Data.Count - 1);
                    gapOffset = isLast ? 0 : Gap;
                    size += GetObjectSize(Data[i])[Axis] + gapOffset;
                }
            }
            else
            {
                for (i = _startBorder.Index; i < index; i++)
                {
                    isLast = i == (Data.Count - 1);
                    gapOffset = isLast ? 0 : Gap;
                    size -= GetObjectSize(Data[i])[Axis] + gapOffset;
                }
            }

            PendingScrollOffset = size;
        }

        private void CleanTop()
        {
            TData data;
            var addScroll = ScrollOffset - _lastScrollOffset;
            var oldStartIndex = _startBorder.Index;
            var positionHead = _startBorder.Position + addScroll;
            int i = 0;
            bool isLast = false;
            float gapOffset = 0f;
            bool haveNewStart = false;
            float size = 0f;
            bool hasVisibleArea = false;
            int newIndex = _startBorder.Index;
            float newPosition = _startBorder.Position + addScroll;
            bool isEndReached;

            for (i = oldStartIndex; i < Data.Count; i++)
            {
                data = Data[i];
                isLast = i == (Data.Count - 1);
                gapOffset = isLast ? 0 : Gap;

                size = GetObjectSize(data)[Axis] + gapOffset;

                isEndReached = positionHead > _viewportEndPos;
                hasVisibleArea = positionHead <= _viewportEndPos && positionHead + size >= 0;

                if (isEndReached)
                {
                    break;
                }
                else if (hasVisibleArea)
                {
                    if (!haveNewStart)
                    {
                        newPosition = positionHead;
                        newIndex = i;
                        haveNewStart = true;
                    }

                    ListMiddle(data, positionHead);
                }
                else
                {
                    ExtremeLeft(data);
                }
                positionHead += size;
            }

            _startBorder.Index = newIndex;
            _startBorder.Position = newPosition;
        }

        private void CleanBottom()
        {
            TData data;
            Border endBorder = GetEnd(_startBorder);
            var addScroll = ScrollOffset - _lastScrollOffset;
            var oldStartIndex = _startBorder.Index;
            var positionHead = _startBorder.Position + addScroll;
            int i = 0;
            bool isLast;
            float gapOffset;
            float size = 0f;
            bool hasVisibleArea = false;

            for (i = oldStartIndex; i <= endBorder.Index; i++)
            {
                data = Data[i];
                isLast = i == (Data.Count - 1);
                gapOffset = isLast ? 0 : Gap;

                size = GetObjectSize(data)[Axis] + gapOffset;

                hasVisibleArea = positionHead + size >= 0 && positionHead <= _viewportEndPos;
                
                if (hasVisibleArea)
                {
                    ListMiddle(data, positionHead);
                }
                else
                {
                    ExtremeRight(data);
                }
                positionHead += size;
            }

            positionHead = _startBorder.Position + addScroll;
            int newIndex = _startBorder.Index ;
            float newPosition = _startBorder.Position + addScroll;

            for (i = oldStartIndex - 1; i >= 0; i--)
            {
                data = Data[i];
                isLast = i == (Data.Count - 1);
                gapOffset = isLast ? 0 : Gap;
                size = GetObjectSize(data)[Axis] + gapOffset;

                positionHead -= size;

                hasVisibleArea = positionHead + size >= 0 && positionHead <= _viewportEndPos;

                if (hasVisibleArea)
                {
                    newIndex = i;
                    newPosition = positionHead;
                    ListMiddle(data, positionHead);
                }
                else
                {
                    break;
                }
            }
            _startBorder.Index = newIndex;
            _startBorder.Position = newPosition;
        }
        public Border Bottom = new Border();


        private Border GetEnd(Border from)
        {
            int i;
            float size = 0;
            TData data;
            bool isLast;
            float position = from.Position;
            float gapOffset = 0;

            for (i = from.Index; i < Data.Count; i++)
            {
                data = Data[i];

                if (position >= _viewportEndPos)
                {
                    break;
                }
                isLast = i == (Data.Count - 1);
                gapOffset = isLast ? 0 : Gap;
                size = GetObjectSize(data)[Axis] + gapOffset;
                position += size;
            }
            return new Border(Mathf.Min(i, Data.Count - 1), position);
        }

        public void UpdateItems()
        {
//            ScrollOffset += PendingScrollOffset;

            if (ScrollOffset < _lastScrollOffset)
            {
                CleanTop();
            }
            else if (ScrollOffset > _lastScrollOffset)
            {
                CleanBottom();
            }

            _lastScrollOffset = ScrollOffset;
            PendingScrollOffset = 0;
        }

        private void OnDestroy()
        {
            foreach (KeyValuePair<int, TCell> keyValuePair in DataToCellMap)
            {
                TCell cell = keyValuePair.Value;
                Destroy(cell);
            }
            foreach (KeyValuePair<string, CellPool> keyValuePair in _pools)
            {
                keyValuePair.Value.Destroy();
            }

            DataToCellMap.Clear();
            _pools.Clear();
        }
    }
}