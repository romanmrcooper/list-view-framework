﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.ListView.Examples.Test
{
    public class CellPool
    {
        public readonly List<BaseCell> Pool = new List<BaseCell>();
        public BaseCell Prefab;

        public CellPool(BaseCell prefab)
        {
            if (prefab == null)
            {
                throw new Exception("Template prefab cannot be null");
            }
            Prefab = prefab;
        }

        public void Destroy()
        {
            Prefab = null;

            foreach (BaseCell baseCell in Pool)
            {
                MonoBehaviour.Destroy(baseCell);
            }
            Pool.Clear();
        }
    }
}