﻿using System;

namespace Assets.ListView.Examples.Test
{
    [Serializable]
    public class Border
    {
        public int Index = 0;
        public float Position = 0f;

        public Border()
        {
            
        }
        public Border(int index, float position)
        {
            Index = index;
            Position =  position;
        }
    }
}