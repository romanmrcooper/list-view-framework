﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.ListView.Examples.Test
{
    public class Main : MonoBehaviour
    {
        public TestList List;

        void Start()
        {
            List.TemplateQualifier = Qualifier;
            List.Data = GetFriends(50);

//            List.ScrollTo(8);
//            Invoke("Test", 2f);
        }

        void Test()
        {
            List.ScrollTo(15);
//            Invoke("Test2", 2f);
        }

        void Test2()
        {
            List.ScrollTo(0);
        }

        private string Qualifier(int i, FriendData friendData)
        {
            if (i%2 == 0)
            {
                return "2dItemNormal";
            } 
            else if (i%5 == 0)
            {
                return "2dItemBig";
            }
            return "2dItemSmall";
        }

        List<FriendData> GetFriends(int count)
        {
            List<FriendData> list = new List<FriendData>();
            for (int i = 0; i < count; i++)
            {
                var friend = new FriendData();
                friend.Name = "Index_" + (i);
                list.Add(friend);
            }
            return list;
        }
    }
}