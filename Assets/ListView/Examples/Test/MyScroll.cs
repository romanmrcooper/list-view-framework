﻿using Assets.ListView.Examples.Test;
using UnityEngine;

public class MyScroll : MonoBehaviour
{
    public TestList ListView;
    public float InertiaCoef = 1f;
    public float Threshold = 1f;
    public float stickSeconds = 0.25f;

    private bool _isManual;
    private Vector3 _lastDrag;
    private float _lastOffest;
    private float _speed;
    private float _startOffset;
    private Vector3 _startPosition;
    private float _timeDrag;
    private bool _isSticked;
    private float _stickedSpeed;
    private float _drag;

    public void OnPointerDown()
    {
//        StopScrolling(Input.mousePosition);
    } 

    public void OnBeginDrag()
    {
        StartScrolling(Input.mousePosition);
    }

    public void OnDrag()
    {
        Scroll(Input.mousePosition);
    }

    public void OnEndDrag()
    {
        StopScrolling(Input.mousePosition);
    }

    void Update()
    {
        if (_isManual)
        {
            _timeDrag += Time.deltaTime;
        }
        else
        {
            if (ListView.ScrollOffset > 0)
            {
                _stickedSpeed = -(ListView.ScrollOffset / stickSeconds) * Time.deltaTime;
                int newScroll = Mathf.Max(0, (int)(ListView.ScrollOffset + _stickedSpeed));

                if (newScroll == 0)
                {
                    _stickedSpeed = 0;
                }

                ListView.ScrollOffset = newScroll;
                ListView.UpdateItems();
            }
            else if (ListView.ScrollOffset < ListView.Bottom.Position)
            {
                _stickedSpeed = (_bottmo / stickSeconds) * Time.deltaTime;
                float newScroll = Mathf.Min(ListView.Bottom.Position, ListView.ScrollOffset + _stickedSpeed);
                if (newScroll >= ListView.Bottom.Position)
                {
//                    ListView.ScrollOffset = ListView.Bottom.Position;
                    _bottmo = 0;
//                    _stickedSpeed = 0;
                }
                ListView.ScrollOffset = newScroll;
                ListView.UpdateItems();
            }
            else if (_speed != 0)
            {
                _speed = _speed * InertiaCoef;
                _speed = Mathf.Abs(_speed) <= 0.1 ? 0 : _speed;
                ListView.ScrollOffset += _speed;
                if (ListView.ScrollOffset > 0 || ListView.ScrollOffset < ListView.Bottom.Position)
                    _speed = 0;
                ListView.UpdateItems();
            }
        }
    }

    private void StartScrolling(Vector3 start)
    {
        if (_isManual)
        {
            return;
        }
        _isSticked = false;
        _timeDrag = 0;
        _stickedSpeed = 0;
        _speed = 0;
        Debug.Log("ListView.ScrollOffset " + ListView.ScrollOffset);
        _isManual = true;
        _startPosition = start;
        _startOffset = ListView.ScrollOffset;
    }

    private void Scroll(Vector3 position)
    {
        if (_isManual)
        {
            int axis = ListView.Axis;
            float delta = position[axis] - _startPosition[axis];
            if (axis == 1)
                delta = _startPosition[axis] - position[axis];
            _drag = _startOffset + delta;
//            Debug.Log(delta);
            ListView.ScrollOffset = _startOffset + delta;
            ListView.UpdateItems();
            _lastDrag = position;
        }
    }

    private float _bottmo;

    private void StopScrolling(Vector3 position)
    {
        if (!_isManual)
            return;
        _isManual = false;
//        _bottmo = ListView.GetBottomStick();

        if (ListView.ScrollOffset > 0)
        {
            _speed = 0;
            _timeDrag = 0;
            _isSticked = true;

            _stickedSpeed = -(ListView.ScrollOffset / stickSeconds) * Time.deltaTime;
            Debug.Log("1 End ListView.ScrollOffset " + ListView.ScrollOffset + "      " + _stickedSpeed);
            return;
        }
        else if (ListView.ScrollOffset < ListView.Bottom.Position)
        {
            _bottmo = ListView.Bottom.Position - ListView.ScrollOffset;
            _stickedSpeed = (_bottmo / stickSeconds) * Time.deltaTime;
            Debug.Log("2 End ListView.ScrollOffset " + ListView.ScrollOffset + "      " + _stickedSpeed);
            return;
        }
        Debug.Log("3 End ListView.ScrollOffset " + ListView.ScrollOffset + "      " + _stickedSpeed);

        if (position == _lastDrag)
        {
            _timeDrag = 0;
            _speed = 0;
        }
        else
        {
            _speed = (ListView.ScrollOffset - _startOffset) * _timeDrag * InertiaCoef;
        }

    }
}