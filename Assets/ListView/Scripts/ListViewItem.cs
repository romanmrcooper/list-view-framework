﻿using System;
using UnityEngine;       
using System.Collections.Generic;

namespace ListView
{
    public class ListViewItem : ListViewItem<ListViewItemInspectorData>
    {
    }

//    [RequireComponent(typeof (Collider))]
    public class ListViewItemBase : MonoBehaviour
    {
    }

    public class ListViewItem<ItemDataType> : ListViewItemBase where ItemDataType : ListViewItemData
    {
        public ItemDataType data;

        public virtual void Setup(ItemDataType data)
        {
            this.data = data;
            data.item = this;
        }
    }

    public class ListViewItemData
    {
        public string template;
        public MonoBehaviour item;
    }

//    public class ListViewItemData<DataType>
//    {
//        public DataType data;
//        public Func<int, DataType, string> qualifier;
//        public MonoBehaviour item;
//    }

    public class ListViewItemNestedData<ChildType> : ListViewItemData
    {
        public bool expanded;
        public ChildType[] children;
    }

    [System.Serializable]
    public class ListViewItemInspectorData : ListViewItemData
    {
    }

    public class ListViewItemTemplate
    {
        public readonly GameObject prefab;
        public readonly List<MonoBehaviour> pool = new List<MonoBehaviour>();

        public ListViewItemTemplate(GameObject prefab)
        {
            if (prefab == null)
                Debug.LogError("Template prefab cannot be null");
            this.prefab = prefab;
        }
    }
}